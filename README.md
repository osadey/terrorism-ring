Oodrive Innovation - Detect Terrorism suspect activity with the Neo4j Graph Database

The following GitLab Project contains the entire source code and the Neo4j Scripts. 
Please do the following:
a)  check out the following project on GitLab:
	https://gitlab.com/osadey/terrorism-ring.git
b)	Build it with a mvn install
c)	Start Neo4j, create a new database and open the console
d)	Run the following Neo4j Scripts (by copy/paste into the console command line)
	a.	Clear the database (if needed): /neo4j-scripts/populate/01-clear database.txt
	b.	Populate the database: /neo4j-scripts/populate/02-populate.txt
	c.	Copy the Phone Calls Record CSV file into a folder of your local disk: This file is located in neo4j-scripts/populate/TerroPhoneCalls.csv
	d.	Import the Phone Calls Record CSV File into the database. Firstly, you’ll have to adapt the path of the following script regarding the previous step: neo4j-scripts/populate/03-import phone call records.txt 
e)	Run the following Java Class located at the following path of your Java Project: /src/main/java/com/oodrive/graphdatabase/terrorismdetection/TerroristRing.java
